# BABKit

[![CI Status](http://img.shields.io/travis/Bryn Bodayle/BABKit.svg?style=flat)](https://travis-ci.org/Bryn Bodayle/BABKit)
[![Version](https://img.shields.io/cocoapods/v/BABKit.svg?style=flat)](http://cocoadocs.org/docsets/BABKit)
[![License](https://img.shields.io/cocoapods/l/BABKit.svg?style=flat)](http://cocoadocs.org/docsets/BABKit)
[![Platform](https://img.shields.io/cocoapods/p/BABKit.svg?style=flat)](http://cocoadocs.org/docsets/BABKit)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

BABKit is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

    pod "BABKit"

## Author

Bryn Bodayle, bryn.bodayle@gmail.com

## License

BABKit is available under the MIT license. See the LICENSE file for more info.

