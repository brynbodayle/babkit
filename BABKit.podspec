Pod::Spec.new do |s|
  s.name             = "BABKit"
  s.version          = "0.1.0"
  s.summary          = "A library of useful things."
  s.description      = <<-DESC
                       A library of useful things. Temporary description.
                       DESC
  s.homepage         = "https://bitbucket.org/brynbodayle/babkit"
  s.license          = 'MIT'
  s.author           = { "Bryn Bodayle" => "bryn.bodayle@gmail.com" }
  s.source           = { :git => "https://bitbucket.org/brynbodayle/babkit", :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/brynbodayle'

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
  s.resource_bundles = {
    'BABKit' => ['Pod/Assets/*.png']
  }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
