//
//  BABAppDelegate.h
//  BABKit
//
//  Created by CocoaPods on 02/19/2015.
//  Copyright (c) 2014 Bryn Bodayle. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BABAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
