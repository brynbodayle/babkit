//
//  BABViewController.m
//  BABKit
//
//  Created by Bryn Bodayle on 02/19/2015.
//  Copyright (c) 2014 Bryn Bodayle. All rights reserved.
//

#import "BABViewController.h"
#import "BABButton.h"


@interface BABViewController ()
@property (weak, nonatomic) IBOutlet BABButton *button;

@end

@implementation BABViewController

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];

}

- (IBAction)buttonPressed:(id)sender {
}

@end
