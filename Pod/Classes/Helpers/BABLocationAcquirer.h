//
//  BABLocationAcquirer.h
//  Pods
//
//  Created by Bryn Bodayle on April/9/2015.
//
//

#import <Foundation/Foundation.h>

@class CLLocation;

@interface BABLocationAcquirer : NSObject

@property (nonatomic, readonly, getter=isLocationAuthorized) BOOL locationAuthorized;

- (void)requestLocation:(void (^)(CLLocation *location))completionBlock;
- (void)cancel;

@end
