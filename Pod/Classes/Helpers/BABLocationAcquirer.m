//
//  BABLocationAcquirer.m
//  Pods
//
//  Created by Bryn Bodayle on April/9/2015.
//
//

#import "BABLocationAcquirer.h"

@import CoreLocation;

@interface BABLocationAcquirer()<CLLocationManagerDelegate>

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) NSMutableArray *locationRequestCompletionBlocks;

@end

@implementation BABLocationAcquirer

- (id)init {
    
    self = [super init];
    if (self) {
        
        _locationRequestCompletionBlocks = [NSMutableArray array];
        
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        _locationManager.activityType = CLActivityTypeOther;
        
        NSBundle *mainBundle = [NSBundle mainBundle];
        
        NSString *alwaysUsageDescription = [mainBundle objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"];
        NSString *whenInUseUsageDescription = [mainBundle objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"];
        
        NSAssert(alwaysUsageDescription.length > 0 || whenInUseUsageDescription.length > 0, @"Application should provide either a \"when in use\" usage description or an \"always\" usage description in the Info.plist.");
        
    }
    return self;
}

- (void)requestLocation:(void (^)(CLLocation *location))completionBlock {
    
    [self.locationRequestCompletionBlocks addObject:[completionBlock copy]];
}


- (void)cancel {
    
    
}

#pragma mark - Setters & Getters

- (BOOL)isLocationAuthorized {
    
    CLAuthorizationStatus authorizationStatus = [CLLocationManager authorizationStatus];
    
    return authorizationStatus == kCLAuthorizationStatusAuthorized || authorizationStatus == kCLAuthorizationStatusAuthorizedAlways || authorizationStatus == kCLAuthorizationStatusAuthorizedWhenInUse;
}

@end
