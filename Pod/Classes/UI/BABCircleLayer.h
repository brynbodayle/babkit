//
//  BABCircleLayer.h
//  Pods
//
//  Created by Bryn Bodayle on March/16/2016.
//
//

#import <QuartzCore/QuartzCore.h>

@interface BABCircleLayer : CALayer

@property (nonatomic, assign) CGFloat fillPercentage;
@property (nonatomic, strong) UIColor *fillColor;

@end
