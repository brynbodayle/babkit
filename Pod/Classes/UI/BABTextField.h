//
//  BABTextField.h
//  Pods
//
//  Created by Bryn Bodayle on March/5/2015.
//
//

#import <UIKit/UIKit.h>

@interface BABTextField : UITextField

@property (nonatomic, strong) IBInspectable UIColor *placeholderColor;
@property (nonatomic, assign) UIEdgeInsets editingRectInsets;
@property (nonatomic, assign) UIEdgeInsets textRectInsets;

@end
