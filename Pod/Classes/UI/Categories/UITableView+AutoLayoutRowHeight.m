//
//  UITableView+AutoLayoutRowHeight.m
//  Pods
//
//  Created by Bryn Bodayle on February/26/2015.
//
//

#import "UITableView+AutoLayoutRowHeight.h"

@implementation UITableView (AutoLayoutRowHeight)

- (CGFloat)heightForPrototypeCell:(UITableViewCell *)cell cellConfigurationBlock:(void (^)(UITableViewCell *cell))cellConfigurationBlock {
    
    if(cellConfigurationBlock) {
        
        cellConfigurationBlock(cell);
    }
    
    [cell setNeedsUpdateConstraints];
    [cell updateConstraintsIfNeeded];
    
    cell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(self.bounds), CGRectGetHeight(self.bounds));
    
    [cell setNeedsLayout];
    [cell layoutIfNeeded];
    
    CGSize size = [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    
    CGFloat separatorHeight = self.separatorStyle == UITableViewCellSeparatorStyleSingleLine ? 1.0f : 0.0f;
    
    return size.height + separatorHeight;
}

@end
