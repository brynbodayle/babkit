//
//  UITableView+AutoLayoutRowHeight.h
//  Pods
//
//  Created by Bryn Bodayle on February/26/2015.
//
//

#import <UIKit/UIKit.h>

@interface UITableView (AutoLayoutRowHeight)

- (CGFloat)heightForPrototypeCell:(UITableViewCell *)cell cellConfigurationBlock:(void (^)(UITableViewCell *cell))cellConfigurationBlock;

@end
