//
//  BABCircleLayer.m
//  Pods
//
//  Created by Bryn Bodayle on March/16/2016.
//
//

#import "BABCircleLayer.h"

static CGFloat BABCircleLayerDegreesToRadians(CGFloat angle) {
    
    return ((angle) / 180.0 * M_PI);
}

static CGFloat const BABCircleLayerAngleOffset = -90.0f;

@implementation BABCircleLayer

@dynamic fillPercentage;

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        [self setNeedsDisplay];
    }
    return self;
}

+ (BOOL)needsDisplayForKey:(NSString *)key {
    
    if([@"fillPercentage" isEqualToString:key]) {
        
        return YES;
    }
    
    return [super needsDisplayForKey:key];
}

- (void)display {
    
    CGFloat fillPercentage = self.presentationLayer ? [self.presentationLayer fillPercentage] : [self fillPercentage];
    
    CGRect rect = self.bounds;
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGPoint center = CGPointMake(CGRectGetMidX(rect), CGRectGetMidY(rect));
    CGFloat radius = center.y;
    CGFloat angle = BABCircleLayerDegreesToRadians((360.0f * fillPercentage) + BABCircleLayerAngleOffset);
    CGPoint points[3] = {
        CGPointMake(center.x, 0.0f),
        center,
        CGPointMake(center.x + radius * cosf((float)angle), center.y + radius * sinf((float)angle))
    };
    
    if (fillPercentage > 0.0f) {
        
        CGContextSetFillColorWithColor(context, self.fillColor.CGColor);
        CGContextAddLines(context, points, sizeof(points) / sizeof(points[0]));
        CGContextAddArc(context, center.x, center.y, radius, BABCircleLayerDegreesToRadians(BABCircleLayerAngleOffset), angle, NO);
        CGContextDrawPath(context, kCGPathEOFill);
        CGContextFillPath(context);
    }
    
    self.contents = (id)UIGraphicsGetImageFromCurrentImageContext().CGImage;
    UIGraphicsEndImageContext();
}

@end
