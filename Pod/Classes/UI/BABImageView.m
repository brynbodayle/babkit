//
//  BABImageView.m
//  Pods
//
//  Created by Bryn Bodayle on January/26/2016.
//
//

#import "BABImageView.h"

@implementation BABImageView

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    if(self.imageRenderingModeAlwaysTemplate && self.image) {
        
        self.image = [self.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self tintColorDidChange];
    }
}

@end
