//
//  BABGradientView.m
//  Pods
//
//  Created by Bryn Bodayle on January/6/2016.
//
//

#import "BABGradientView.h"

NSArray *BABGradientViewCGColorsFromUIColorsArray(NSArray *colors) {
    
    NSMutableArray *CGColors = [NSMutableArray arrayWithCapacity:colors.count];
    
    for(UIColor *color in colors) {
        
        [CGColors addObject:(id)color.CGColor];
    }
    
    return CGColors;
}

@interface BABGradientView()

@property (nonatomic, assign) CGRect drawnGradientFrame;
@property (nonatomic, assign) BOOL shouldDrawGradient;

@end

@implementation BABGradientView

#pragma mark - Initialization

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self sharedInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        
        [self sharedInit];
    }
    return self;
}

- (void)sharedInit {
    
    self.backgroundColor = [UIColor clearColor];
    self.opaque = NO;
    
    //Default is a vertical gradient clear to white gradient
    _startPoint = CGPointMake(0.5f, 0.0f);
    _endPoint = CGPointMake(0.5f, 1.0f);
    _locations = @[@0, @1.0f];
    _colors = @[[UIColor lightGrayColor], [UIColor whiteColor]];
    _drawnGradientFrame = CGRectZero;
    
    [self setNeedsDisplay];
}

#pragma mark - Setters & Getters

- (void)setStartPoint:(CGPoint)startPoint {
    
    _startPoint = startPoint;
    _shouldDrawGradient = YES;
    [self setNeedsDisplay];
}

- (void)setEndPoint:(CGPoint)endPoint {

    _endPoint = endPoint;
    _shouldDrawGradient = YES;
    [self setNeedsDisplay];
}

- (void)setLocations:(NSArray *)locations {
    
    _locations = locations;
    _shouldDrawGradient = YES;
    [self setNeedsDisplay];
}

- (void)setColors:(NSArray *)colors {
    
    _colors = colors;
    _shouldDrawGradient = YES;
    [self setNeedsDisplay];

}

#pragma mark - Drawing

- (void)drawRect:(CGRect)rect {
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    NSArray *CGColors = BABGradientViewCGColorsFromUIColorsArray(self.colors);
    
    CGFloat gradientLocations[self.locations.count];
    
    for(NSUInteger index = 0; index < self.locations.count; index++) {
        
        gradientLocations[index] = [self.locations[index] doubleValue];
    }
    
    CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef)CGColors, gradientLocations);
    
    CGColorSpaceRelease(colorSpace);
    
    CGFloat rectWidth = CGRectGetWidth(rect);
    CGFloat rectHeight = CGRectGetHeight(rect);
    
    CGPoint startPoint = CGPointMake(self.startPoint.x * rectWidth, self.startPoint.y * rectHeight);
    CGPoint endPoint = CGPointMake(self.endPoint.x * rectWidth, self.endPoint.y * rectHeight);

    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, 0);
    CGGradientRelease(gradient);
}

@end
