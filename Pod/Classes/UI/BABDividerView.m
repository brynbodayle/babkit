//
//  BABDividerView.m
//  Pods
//
//  Created by Bryn Bodayle on February/19/2015.
//
//

#import "BABDividerView.h"

@implementation BABDividerView

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        
        [self sharedInit];
    }
    return self;
}

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [self sharedInit];
}

- (void)sharedInit {
    
    self.opaque = NO;
    self.backgroundColor = [UIColor clearColor];
}

- (void)updateConstraints {
    
    [super updateConstraints];
    
    for(NSLayoutConstraint *constraint in self.constraints) {
        
        if(constraint.firstAttribute == NSLayoutAttributeWidth && constraint.constant == 1) {
            constraint.constant = 0.5f;
        }
        else if(constraint.firstAttribute == NSLayoutAttributeHeight && constraint.constant == 1) {
            
            constraint.constant = 0.5f;
        }
    }
    
}

- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect {
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetAllowsAntialiasing(context, NO);
    CGContextClearRect(context, rect);
    
    [self.dividerColor setFill];
    
    if(rect.size.height > rect.size.width) {
        rect.size.width = 0.5f;
    }
    else {
        
        rect.size.height = 0.5f;
    }
    
    UIRectFill(rect);
    
}


@end
