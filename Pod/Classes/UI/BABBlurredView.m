//
//  BABBlurredView.m
//  Pods
//
//  Created by Bryn Bodayle on April/9/2015.
//
//

#import "BABBlurredView.h"

@interface BABBlurredView()

@property (nonatomic, strong) UIToolbar *toolbar;

@end

@implementation BABBlurredView

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.toolbar = [[UIToolbar alloc] initWithFrame:self.bounds];
    self.toolbar.translatesAutoresizingMaskIntoConstraints = NO;
    self.toolbar.clipsToBounds = YES;

    UIView *blurToolbar = self.toolbar;
    
    [self insertSubview:self.toolbar atIndex:0];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[blurToolbar]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(blurToolbar)]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[blurToolbar]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(blurToolbar)]];
}

@end
