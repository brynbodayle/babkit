//
//  BABGradientView.h
//  Pods
//
//  Created by Bryn Bodayle on January/6/2016.
//
//

#import <UIKit/UIKit.h>

@interface BABGradientView : UIView

@property (nonatomic, assign) CGPoint startPoint;
@property (nonatomic, assign) CGPoint endPoint;
@property (nonatomic, strong) NSArray *locations;
@property (nonatomic, strong) NSArray *colors;

@end
