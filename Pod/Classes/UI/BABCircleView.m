//
//  BABCircleView.m
//  Pods
//
//  Created by Bryn Bodayle on February/19/2015.
//
//

#import "BABCircleView.h"

@interface BABCircleView()

@property (nonatomic, assign) CGRect drawnRect;

@end

@implementation BABCircleView

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        
        [self sharedInit];
    }
    return self;
}

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [self sharedInit];
}

- (void)sharedInit {
    
    self.opaque = NO;
}

- (void)setFillColor:(UIColor *)fillColor {
    
    _fillColor = fillColor;
    
    [self setNeedsDisplay];
}

- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    if(!CGRectEqualToRect(self.bounds, self.drawnRect)) {
        
        [self setNeedsDisplay];
    }
}

- (void)drawRect:(CGRect)rect {
    
    CGFloat width = CGRectGetWidth(self.bounds);
    CGFloat height = CGRectGetHeight(self.bounds);
    
    CGFloat diameter = width > height ? height : width;
    
    CGRect centerRect = CGRectMake((width - diameter)/2.0f, (height - diameter)/2.0f, diameter, diameter);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextAddEllipseInRect(context, centerRect);
    CGContextSetFillColor(context, CGColorGetComponents(self.fillColor.CGColor));
    CGContextFillPath(context);
    
    self.drawnRect = rect;
}

@end
