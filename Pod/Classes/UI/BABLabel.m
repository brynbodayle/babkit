//
//  BABLabel.m
//  Pods
//
//  Created by Bryn Bodayle on May/21/2015.
//
//

#import "BABLabel.h"

@implementation BABLabel

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    if(self.kerning != 0.0f) {
        
        [self updateAttributedString];
    }
    
    if(self.paragraphLineSpacing != 0.0f) {
        
        [self updateAttributedString];
    }
}

- (void)setText:(NSString *)text {
    
    [super setText:text];
    
    [self updateAttributedString];
}

- (void)setTextColor:(UIColor *)textColor {
    
    [super setTextColor:textColor];
    
    [self updateAttributedString];
}

- (void)setFont:(UIFont *)font {
    
    [super setFont:font];
    
    [self updateAttributedString];
}

- (void)setTextAlignment:(NSTextAlignment)textAlignment {
    
    [super setTextAlignment:textAlignment];
    
    [self updateAttributedString];
}

- (void)setKerning:(CGFloat)kerning {
    
    _kerning = kerning;
    
    [self updateAttributedString];
}

- (void)setParagraphLineSpacing:(CGFloat)paragraphLineSpacing {
    
    _paragraphLineSpacing = paragraphLineSpacing;
    [self updateAttributedString];
}

- (void)updateAttributedString {
    
    if(!self.text) {
        
        return;
    }
    
    NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraphStyle.alignment = self.textAlignment;
    paragraphStyle.lineSpacing = self.paragraphLineSpacing;
    
    NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
    
    attributes[NSForegroundColorAttributeName] = self.textColor;
    attributes[NSFontAttributeName] = self.font;
    attributes[NSKernAttributeName] = @(self.kerning);
    attributes[NSParagraphStyleAttributeName] = paragraphStyle;
    
    self.attributedText = [[NSAttributedString alloc] initWithString:self.text attributes:attributes];
}

@end
