//
//  BABButton.h
//  Pods
//
//  Created by Bryn Bodayle on February/19/2015.
//
//

#import <UIKit/UIKit.h>

@interface BABButton : UIButton

@property (nonatomic, assign) IBInspectable BOOL underlineTitleLabel;
@property (nonatomic, assign) IBInspectable CGFloat kerning;

@property (nonatomic, assign) IBInspectable BOOL verticallyFlipsImage;
@property (nonatomic, assign) IBInspectable BOOL horizontallyFlipsImage;

@property (nonatomic, assign) IBInspectable BOOL imageRenderingModeAlwaysTemplate;
@property (nonatomic, assign) IBInspectable BOOL imageRenderingModeAlwaysOriginal;

@property (nonatomic, assign) IBInspectable BOOL cornerRadiusEqualToHalfWidth;
@property (nonatomic, assign) IBInspectable BOOL cornerRadiusEqualToHalfHeight;
@property (nonatomic, assign) IBInspectable CGFloat cornerRadius;

@property (nonatomic, assign) IBInspectable BOOL centerImageAboveTitleLabel;
@property (nonatomic, assign) IBInspectable BOOL centerImageBelowTitleLabel;
@property (nonatomic, assign) IBInspectable CGFloat imageTitleLabelVerticalSpacing;

@property (nonatomic, assign, getter=isLoading) IBInspectable BOOL loading;
@property (nonatomic, readonly) UIActivityIndicatorView *activityIndicatorView;

@property (nonatomic, strong) IBInspectable UIColor *normalBackgroundColor;
@property (nonatomic, strong) IBInspectable UIColor *highlightedBackgroundColor;
@property (nonatomic, strong) IBInspectable UIColor *selectedBackgroundColor;
@property (nonatomic, strong) IBInspectable UIColor *disabledBackgroundColor;

- (void)setBackgroundColor:(UIColor *)backgroundColor forState:(UIControlState)state;


@end
