//
//  BABImageView.h
//  Pods
//
//  Created by Bryn Bodayle on January/26/2016.
//
//

#import <UIKit/UIKit.h>

@interface BABImageView : UIImageView

@property (nonatomic, assign) IBInspectable BOOL imageRenderingModeAlwaysTemplate;

@end
