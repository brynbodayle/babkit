//
//  BABTextField.m
//  Pods
//
//  Created by Bryn Bodayle on March/5/2015.
//
//

#import "BABTextField.h"

@implementation BABTextField

- (void)setPlaceholderColor:(UIColor *)placeholderColor {
    
    _placeholderColor = placeholderColor;
    
    if(self.placeholder) {
        
        [self updatePlaceholderText];
    }
}

- (void)setPlaceholder:(NSString *)placeholder {
    
    [super setPlaceholder:placeholder];
    
    if(placeholder) {
        
        [self updatePlaceholderText];
    }
}

- (void)setTextRectInsets:(UIEdgeInsets)textRectInsets {
    
    _textRectInsets = textRectInsets;
    [self setNeedsLayout];
}

- (void)setEditingRectInsets:(UIEdgeInsets)editingRectInsets {
    
    _editingRectInsets = editingRectInsets;
    [self setNeedsLayout];
}

#pragma mark -

- (void)updatePlaceholderText {
    
    UIColor *placeholderColor = self.placeholderColor ? : self.textColor;
    
    NSDictionary *placeholderAttributes = @{NSFontAttributeName : self.font, NSForegroundColorAttributeName : placeholderColor};
    NSAttributedString *attributedPlaceholderString = [[NSAttributedString alloc] initWithString:self.placeholder attributes:placeholderAttributes];
    
    self.attributedPlaceholder = attributedPlaceholderString;
}

#pragma mark - Overwritten Methods

- (CGRect)textRectForBounds:(CGRect)bounds {
    
    CGRect rect = [super textRectForBounds:bounds];
    return UIEdgeInsetsInsetRect(rect, self.textRectInsets);
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    
    CGRect rect = [super editingRectForBounds:bounds];
    return UIEdgeInsetsInsetRect(rect, self.editingRectInsets);
}

@end
