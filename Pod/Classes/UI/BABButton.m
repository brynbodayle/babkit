//
//  BABButton.m
//  Pods
//
//  Created by Bryn Bodayle on February/19/2015.
//
//

#import "BABButton.h"

@interface BABButton()

@property (nonatomic, strong) NSMutableDictionary *backgroundColorsForStates;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicatorView;

@end

@implementation BABButton

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self sharedInit];
    }
    return self;
}

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    if(self.imageRenderingModeAlwaysOriginal) {
        
        UIImage *image = [[self imageForState:UIControlStateNormal] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        [self setImage:image forState:UIControlStateNormal];
    }
    else if(self.imageRenderingModeAlwaysTemplate) {
        
        UIImage *image = [[self imageForState:UIControlStateNormal] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self setImage:image forState:UIControlStateNormal];
    }
    
    [self sharedInit];
}

- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    [self updateCornerRadius];
    [self updateIconTitleLabelAlignment];
    
    [self bringSubviewToFront:_activityIndicatorView];
    
    if(self.verticallyFlipsImage && self.horizontallyFlipsImage) {
        
        self.imageView.transform = CGAffineTransformMakeScale(-1.0f, -1.0f);
    }
    else if(self.verticallyFlipsImage) {
        
        self.imageView.transform = CGAffineTransformMakeScale(-1.0f, 1.0f);
    }
    else if(self.horizontallyFlipsImage) {
        
        self.imageView.transform = CGAffineTransformMakeScale(1.0f, -1.0f);
    }
}

- (void)sharedInit {
    
    [self updateBackgroundColorForCurrentState];
    [self updateTitleLabel];
    [self updateIconTitleLabelAlignment];
    [self updateLoadingState];
}

- (void)tintColorDidChange {
    
    [super tintColorDidChange];
    
    _activityIndicatorView.color = self.tintColor;
}

- (void)setTitle:(NSString *)title forState:(UIControlState)state {
    
    [super setTitle:title forState:state];
    
    [self updateTitleLabel];
}

- (void)setTitleColor:(UIColor *)color forState:(UIControlState)state {
    
    [super setTitleColor:color forState:state];
    
    [self updateTitleLabel];
}

- (void)setTitleShadowColor:(UIColor *)color forState:(UIControlState)state {
    
    [super setTitleShadowColor:color forState:state];
    
    [self updateTitleLabel];
}

#pragma mark - Setters & Getters

- (void)setUnderlineTitleLabel:(BOOL)underlineTitleLabel {
    
    _underlineTitleLabel = underlineTitleLabel;
    
    [self updateTitleLabel];
}

- (void)setKerning:(CGFloat)kerning {
    
    _kerning = kerning;
    
    [self updateTitleLabel];
}

- (void)setCornerRadius:(CGFloat)cornerRadius {
    
    _cornerRadius = cornerRadius;
    
    [self updateCornerRadius];
}

- (NSMutableDictionary *)backgroundColorsForStates {
    
    if(!_backgroundColorsForStates) {
        
        _backgroundColorsForStates = [NSMutableDictionary dictionary];
    }
    
    return _backgroundColorsForStates;
}

- (void)setNormalBackgroundColor:(UIColor *)normalBackgroundColor {
    
    [self setBackgroundColor:normalBackgroundColor forState:UIControlStateNormal];
    
    _normalBackgroundColor = normalBackgroundColor;
}

- (void)setHighlightedBackgroundColor:(UIColor *)highlightedBackgroundColor {
    
    [self setBackgroundColor:highlightedBackgroundColor forState:UIControlStateHighlighted];
    
    _highlightedBackgroundColor = highlightedBackgroundColor;
}

- (void)setSelectedBackgroundColor:(UIColor *)selectedBackgroundColor {
    
    [self setBackgroundColor:selectedBackgroundColor forState:UIControlStateSelected];
    
    _selectedBackgroundColor = selectedBackgroundColor;
}

- (void)setDisabledBackgroundColor:(UIColor *)disabledBackgroundColor {
    
    [self setBackgroundColor:disabledBackgroundColor forState:UIControlStateDisabled];
    
    _disabledBackgroundColor = disabledBackgroundColor;
}

- (void)setBackgroundColor:(UIColor *)backgroundColor forState:(UIControlState)state {
    
    self.backgroundColorsForStates[@(state)] = backgroundColor;
    
    [self updateBackgroundColorForCurrentState];
}

- (void)setEnabled:(BOOL)enabled {
    
    [super setEnabled:enabled];
    
    [self updateBackgroundColorForCurrentState];
}

- (void)setHighlighted:(BOOL)highlighted {
    
    [super setHighlighted:highlighted];
    
    [self updateBackgroundColorForCurrentState];
    
    UIColor *normalColor = [self titleColorForState:UIControlStateNormal];
    UIColor *highlightedColor = [self titleColorForState:UIControlStateHighlighted];
    
    if(!highlightedColor || [normalColor isEqual:highlightedColor]) {
        
        NSTimeInterval animationDuration = highlighted ? 0.1f : 0.3f;
        CGFloat newAlpha = highlighted ? 0.2f : 1.0f;
        
        [UIView animateWithDuration:animationDuration delay:0.0f options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionAllowUserInteraction animations:^{
            
            self.titleLabel.alpha = newAlpha;
            self.imageView.alpha = newAlpha;
            
        } completion:nil];
    }
}

- (void)setSelected:(BOOL)selected {
    
    [super setSelected:selected];
    
    [self updateBackgroundColorForCurrentState];
}

- (UIActivityIndicatorView *)activityIndicatorView {
    
    if(!_activityIndicatorView) {
        
        _activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        _activityIndicatorView.translatesAutoresizingMaskIntoConstraints = NO;
        _activityIndicatorView.color = self.tintColor;
        
        CGFloat padding = 5.0f;
        BOOL tooWide = CGRectGetWidth(_activityIndicatorView.bounds) + (padding * 2.0f) > CGRectGetWidth(self.bounds);
        BOOL tooTall = CGRectGetHeight(_activityIndicatorView.bounds) + (padding * 2.0f) > CGRectGetHeight(self.bounds);
        
        if(tooWide || tooTall) {
            
            _activityIndicatorView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
            _activityIndicatorView.color = self.tintColor;
            [_activityIndicatorView sizeToFit];
        }
        
        _activityIndicatorView.hidesWhenStopped = YES;
        [self addSubview:_activityIndicatorView];
        
        NSLayoutConstraint *centerXConstraint = [NSLayoutConstraint constraintWithItem:_activityIndicatorView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f];
        NSLayoutConstraint *centerYConstraint = [NSLayoutConstraint constraintWithItem:_activityIndicatorView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f];
        [self addConstraints:@[centerXConstraint, centerYConstraint]];
        
        [UIView performWithoutAnimation:^{
            
            [self layoutIfNeeded];
        }];
    }
    
    return _activityIndicatorView;
}

- (void)setLoading:(BOOL)loading {
    
    _loading = loading;
    
    [self updateLoadingState];
}


#pragma mark - View Updating

- (void)updateBackgroundColorForCurrentState {
    
    UIColor *backgroundColor = self.backgroundColorsForStates[@(self.state)];
    
    if(backgroundColor) {
        
        self.backgroundColor = backgroundColor;
    }
}

- (void)updateCornerRadius {
    
    CGFloat cornerRadius = self.cornerRadius;
    
    NSAssert(!(self.cornerRadiusEqualToHalfHeight && self.cornerRadiusEqualToHalfWidth), @"Corner radius can not depend on both width and height");
    
    if(self.cornerRadiusEqualToHalfHeight) {
        
        cornerRadius = CGRectGetHeight(self.bounds)/2.0f;
    }
    else if(self.cornerRadiusEqualToHalfWidth) {
        
        cornerRadius = CGRectGetWidth(self.bounds)/2.0f;
    }
    
    if(cornerRadius == 0.0f) {
        
        self.clipsToBounds = NO;
    }
    else {
        
        self.layer.cornerRadius = cornerRadius;
        self.clipsToBounds = NO;
    }
}

- (void)updateIconTitleLabelAlignment {
    
    CGFloat spacing = self.imageTitleLabelVerticalSpacing;
    
    if(self.centerImageAboveTitleLabel) {
        
        CGSize imageSize = self.imageView.image.size;
        self.titleEdgeInsets = UIEdgeInsetsMake(0.0f, - imageSize.width, - (imageSize.height + spacing), 0.0f);
        
        CGSize titleSize = [self.titleLabel.attributedText boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX) options:(NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin) context:NULL].size;
        self.imageEdgeInsets = UIEdgeInsetsMake(- (titleSize.height + spacing), 0.0, 0.0, - titleSize.width);
        
    }
    else if(self.centerImageBelowTitleLabel) {
        
        CGSize imageSize = self.imageView.image.size;
        self.titleEdgeInsets = UIEdgeInsetsMake(0.0f, - imageSize.width, - (imageSize.height + spacing), 0.0f);
        
        CGSize titleSize = [self.titleLabel.attributedText boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX) options:(NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin) context:NULL].size;
        self.imageEdgeInsets = UIEdgeInsetsMake(- (titleSize.height + spacing), 0.0, 0.0, - titleSize.width);
    }
    
}

- (void)updateTitleLabel {
    
    [self updateTitleLabelForState:UIControlStateNormal];
    [self updateTitleLabelForState:UIControlStateHighlighted];
    [self updateTitleLabelForState:UIControlStateDisabled];
    [self updateTitleLabelForState:UIControlStateSelected];
    [self updateTitleLabelForState:UIControlStateApplication];
        
    [self layoutIfNeeded];
}

- (void)updateTitleLabelForState:(UIControlState)state {
    
    NSString *string = [self titleForState:state];
    
    if(string) {
        
        NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
        attributes[NSFontAttributeName] = self.titleLabel.font;
        attributes[NSForegroundColorAttributeName] = [self titleColorForState:state];
        
        if(self.titleLabel.shadowOffset.width != 0 || self.titleLabel.shadowOffset.height != 0) {
            
            NSShadow *shadow = [[NSShadow alloc] init];
            shadow.shadowOffset = self.titleLabel.shadowOffset;
            shadow.shadowColor = [self titleShadowColorForState:state];
            attributes[NSShadowAttributeName] = shadow;
        }
        
        if(self.underlineTitleLabel) {
            
            attributes[NSUnderlineStyleAttributeName] = @(NSUnderlineStyleSingle);
        }
        
        if(self.kerning != 0.0f) {
            
            attributes[NSKernAttributeName] = @(self.kerning);
        }
        
        NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:string attributes:attributes];
        
        [UIView performWithoutAnimation:^{
            
            [self setAttributedTitle:attributedString forState:state];
        }];
    }
    else {
        
        [self setAttributedTitle:nil forState:state];
    }
}

- (void)updateAttributedString:(NSAttributedString *)attributedString forState:(UIControlState)state {
    
    NSMutableAttributedString *mutableAttribtuedString = [attributedString mutableCopy];
    NSRange attribtuedStringRange = NSMakeRange(0, mutableAttribtuedString.length);
    
    if(self.underlineTitleLabel) {
        
        [mutableAttribtuedString addAttribute:NSUnderlineStyleAttributeName value:@(NSUnderlineStyleSingle) range:attribtuedStringRange];
    }
    else {
        
        [mutableAttribtuedString removeAttribute:NSUnderlineStyleAttributeName range:attribtuedStringRange];
    }
    
    if(self.kerning != 0.0f) {
        
        [mutableAttribtuedString addAttribute:NSKernAttributeName value:@(self.kerning) range:attribtuedStringRange];
    }
    else {
        
        [mutableAttribtuedString removeAttribute:NSKernAttributeName range:attribtuedStringRange];
    }
    
    [UIView performWithoutAnimation:^{
        
        [self setAttributedTitle:mutableAttribtuedString forState:state];
    }];
    
}

- (void)updateLoadingState {
    
    if(self.loading) {
        
        [self.titleLabel removeFromSuperview];
        [self.imageView removeFromSuperview];
    }
    else {
        
        if(!self.titleLabel.superview) {
            [self addSubview:self.titleLabel];
        }
        
        if(!self.imageView.superview) {
            [self addSubview:self.imageView];
        }
    }
    
    if(self.loading) {
        
        if(!self.disabledBackgroundColor) {
            
            self.disabledBackgroundColor = self.normalBackgroundColor;
        }
        
        self.enabled = NO;
        [self.activityIndicatorView startAnimating];
    }
    else {
        
        self.enabled = YES;
        [_activityIndicatorView stopAnimating];
    }
    
    
}

@end
