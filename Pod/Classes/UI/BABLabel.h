//
//  BABLabel.h
//  Pods
//
//  Created by Bryn Bodayle on May/21/2015.
//
//

#import <UIKit/UIKit.h>

@interface BABLabel : UILabel

@property (nonatomic, assign) IBInspectable CGFloat kerning;
@property (nonatomic, assign) IBInspectable CGFloat paragraphLineSpacing;

@end
