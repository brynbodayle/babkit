//
//  BABDividerView.h
//  Pods
//
//  Created by Bryn Bodayle on February/19/2015.
//
//

#import <UIKit/UIKit.h>

@interface BABDividerView : UIView

@property (nonatomic, strong) IBInspectable UIColor *dividerColor;

@end
